core = 7.x
api = 2

projects[] = ctools
projects[] = views
projects[] = advanced_help

projects[] = fontyourface
projects[] = service_links

projects[] = profile2
projects[] = realname

projects[] = wysiwyg

projects[] = module_filter
projects[] = globalredirect
projects[] = pathauto
projects[] = libraries
projects[] = features
projects[] = token
projects[] = date

projects[gdlc_sitewide][download][type] = "git"
projects[gdlc_sitewide][download][url] = "git://git.drupal.org/sandbox/gopherspidey/1686560.git"
projects[gdlc_sitewide][type] = "module"

projects[gdlc_messages][download][type] = "git"
projects[gdlc_messages][download][url] = "git://git.drupal.org/sandbox/gopherspidey/1704450.git"
projects[gdlc_messages][type] = "module"

projects[bluescape][download][type] = "git"
projects[bluescape][download][url] = "git://git.drupal.org/sandbox/gopherspidey/1440570.git"
projects[bluescape][type] = "theme"

; Libraries
; Please fill the following out. Type may be one of get, git, bzr or svn,
; and url is the url of the download.
libraries[jquery.cycle][download][type] = "file"
libraries[jquery.cycle][download][url] = "http://malsup.github.com/jquery.cycle.all.js"
libraries[jquery.cycle][directory_name] = "jquery.cycle"
libraries[jquery.cycle][type] = "library"

