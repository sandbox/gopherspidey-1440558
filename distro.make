; drush make distro.make <target directory>

api = 2
core = 7.x

projects[drupal][type] = core
projects[drupal][version] = 7.15

projects[gdlc][type] = profile
projects[gdlc][download][type] = git
projects[gdlc][download][url] = http://git.drupal.org/sandbox/gopherspidey/1440558.git
projects[gdlc][download][branch] = master
